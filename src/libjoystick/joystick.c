#include "joystick.h"

Joystick * joystick_init(unsigned int id)
{
  Joystick     * joystick = NULL;
  unsigned int   size = 0;
  unsigned int   i;

  SDL_Init(SDL_INIT_JOYSTICK);
  
  size = SDL_NumJoysticks();

  if(size > 0 && id < size) {
    joystick = malloc(sizeof(Joystick));

    if(joystick) {
    
      joystick->joystick = SDL_JoystickOpen(id);

      if(joystick->joystick) {
	joystick->data = NULL;
	joystick->freeData = NULL;
	joystick->buttons = SDL_JoystickNumButtons(joystick->joystick);
	joystick->axes = SDL_JoystickNumAxes(joystick->joystick);
	joystick->buttonBind = malloc(sizeof(buttonBinding) * joystick->buttons);
	joystick->axesBind = malloc(sizeof(axesBinding) * joystick->axes);

	for(i=0;i<joystick->buttons;i++) joystick->buttonBind[i] = NULL;
	for(i=0;i<joystick->axes;i++) joystick->axesBind[i] = NULL;
	
      }
      else {
	free(joystick);
	joystick = NULL;
      }
    }
  }
  
  return joystick;
}

void joystick_setFreeData(Joystick * joystick, void (*freeData)(void *))
{
  if(joystick)
    joystick->freeData = freeData;
}

void joystick_setData(Joystick * joystick, void * data)
{
  if(joystick)
    joystick->data = data;
}

void * joystick_getData(Joystick * joystick)
{
  if(joystick)
    return joystick->data;

  return NULL;
}

void joystick_close(Joystick * joystick)
{
  if(joystick) {
    if(joystick->joystick) {
      SDL_JoystickClose(joystick->joystick);
    }

    if(joystick->freeData) {
      joystick->freeData(joystick->data);
    }

    if(joystick->buttonBind) {
      free(joystick->buttonBind);
    }

    if(joystick->axesBind) {
      free(joystick->axesBind);
    }

    free(joystick);
    joystick = NULL;
  }
}

void joystick_closeSDL()
{
  SDL_Quit();
}

void joystick_bindButton(Joystick * joystick, unsigned int id, buttonBinding fct)
{
  if(joystick && id < joystick->buttons) {
    joystick->buttonBind[id] = fct;
  }
}

void joystick_bindAxes(Joystick * joystick, unsigned int id, axesBinding fct)
{
  if(joystick && id < joystick->axes) {
    joystick->axesBind[id] = fct;
  }
}

void joystick_update(Joystick * joystick)
{
  unsigned int i;

  if(joystick) {
    
  SDL_JoystickUpdate();
  
  for(i = 0; i < joystick->buttons; i++) 
    if(joystick->buttonBind[i]) 
      joystick->buttonBind[i](i,SDL_JoystickGetButton(joystick->joystick, i), joystick->data);

  for(i = 0; i < joystick->axes; i++)
    if(joystick->axesBind[i])
      joystick->axesBind[i](i,SDL_JoystickGetAxis(joystick->joystick, i)/32168.f, joystick->data);
  }
}

#ifndef LIBJOYSTICK_H
#define LIBJOYSTICK_H

#include <SDL2/SDL.h>

/**
 *\brief Type of the function you want to bind to a button
 *\param Button_id
 *\param Value 1 = pushed, 0 = non pushed
 *\param Data
 */

typedef void (*buttonBinding)(unsigned int,unsigned int,void *);

/**
 *\brief Type of the function you want to bind to an axes
 *\param Axes_id
 *\param Value Between -1 and 1
 *\param Data
 */

typedef void (*axesBinding)(unsigned int,float,void *);

typedef struct 
{
  SDL_Joystick * joystick;
  void         * data;
  unsigned int   buttons; /* Number of buttons */
  unsigned int   axes; /* Number of axes */
  
  buttonBinding * buttonBind;
  axesBinding   * axesBind;
  
  void (*freeData)(void *);
}Joystick;

/**
 *\brief Open a joystick and return a pointer on it
 *\param id Id of the joystick. The first hoystick is number 0.
 *\return The joystick or NULL if an error occured
 */

Joystick * joystick_init(unsigned int id);

/**
 *\brief Set un function to free the data of the joystick when the joystick is closed
 *\param joystick The joystick
 *\param freeData The function to free data
 */

void joystick_setFreeData(Joystick * joystick, void (*freeData)(void *));

/**
 *\brief Set data to a joystick
 *\param joystick The joystick
 *\param data The data you want to add
 */

void joystick_setData(Joystick * joystick, void * data);

/**
 *\brief Close a joystick
 *\param The joystic to close
 */

void joystick_close(Joystick * joystick);

/**
 *\brief Joystick are managed by the SDL library. Use this function if you use only this joystick library at the end of the program.
 */

void joystick_closeSDL();

/**
 *\brief Get the data of a joystick
 *\param joystick The joystick
 *\return The data of the joystick or NULL if there were no data
 */

void * joystick_getData(Joystick * joystick);

/**
 *\brief bind a button to a specific function
 *\param joystick The joystick
 *\param id The button number
 *\param fct The function you want to bind.
 */

void joystick_bindButton(Joystick * joystick, unsigned int id, buttonBinding fct);

/**
 *\brief bind an axes to a specific function
 *\param joystick The joystick
 *\param id The axes
 *\param fct The function you want to bind
 */

void joystick_bindAxes(Joystick * joystick, unsigned int id, axesBinding fct);

/**
 *\brief Update the state of the joystick. Use at each turn of the event loop.
 *\param joystick The joystick
 */

void joystick_update(Joystick * joystick);

#ifdef BORNE_PIXEL

#define NB_BUTTON_BORNE 6
#define NB_AXE_BORNE 2

#define JOY1 joystick1
#define JOY2 joystick2

#define JOY1_AXE_X 0
#define JOY1_AXE_Y 1

#define JOY2_AXE_X 0
#define JOY2_AXE_Y 1

#define JOY1_BTN1 0
#define JOY1_BTN2 1
#define JOY1_BTN3 2
#define JOY1_BTN4 3
#define JOY1_BTN5 4
#define JOY1_BTN6 4

#define JOY2_BTN1 0
#define JOY2_BTN2 1
#define JOY2_BTN3 2
#define JOY2_BTN4 3
#define JOY2_BTN5 4
#define JOY2_BTN6 5

/* Joystick 1 */

#define BTN_COIN 9
#define BTN_PLAYER1 7
#define BTN_PLAYER2 6
#define BTN_QUIT 8

#define Bind_Player1(F) joystick_bindButton(JOY,BTN_PLAYER1,F);
#define Bind_Player2(F) joystick_bindButton(JOY1,BTN_PLAYER2,F);
#define Bind_Coin(F) joystick_bindButton(JOY1,BTN_COIN,F);
#define Bind_Quit(F) joystick_bindButton(JOY1,BTN_QUIT,F);

#endif

#endif /* LIBJOYSTICK_H */


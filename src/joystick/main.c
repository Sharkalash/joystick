
#include <stdlib.h>
#include <stdio.h>

#define BORNE_PIXEL
#include <joystick.h>

void press(unsigned int id, unsigned int val, void * data)
{
  int * d = (int *)data;
  
  printf("%d %d\n", id, *d);

  (*d) += val;
}

void joy(unsigned int id, float val, void * data)
{
  data = (int *)data;
  printf("%d %f\n", id,val);
}

int main()
{
  Joystick * JOY1, * JOY2;
  int * bla = malloc(sizeof(int));
  int * alb = malloc(sizeof(int));

  *bla = 8;
  
  JOY1 = joystick_init(0);
  JOY2 = joystick_init(1);

  joystick_setData(JOY1, bla);
  joystick_setData(JOY2, alb);
  joystick_setFreeData(JOY1, free);
  joystick_setFreeData(JOY2, free);

  for(int i = 0; i < NB_BUTTON_BORNE; i++) {
    joystick_bindButton(JOY1, i, press);
    joystick_bindButton(JOY2, i, press);
  }
  
  while(1) {
    joystick_update(JOY1);
    joystick_update(JOY2);
  }

  joystick_close(JOY1);
  joystick_close(JOY2);
  joystick_closeSDL();
  
  return 0;
}

# Joystick

Wrapper SDL Joystick

## Borne Pixel

Add

> #define BORNE_PIXEL

before including the joystick lib and you will have the mapping of the joystick of the BORNE.

> Joystick * JOY1, * JOY2; /* Declare the joystick */

Declare them that way with this name.